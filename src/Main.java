import demo.DemoService;
import service.AbiturientService;

public class Main {

    public static void main(String[] args) {
        new DemoService(new AbiturientService()).execute();
    }
}
