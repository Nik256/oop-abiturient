package demo;

import entity.Abiturient;

public interface IDemoService {
    void checkGettingAbiturientsWhoEnteredToTheUniversity(Abiturient[] abiturients, int totalNumberOfStudents);
    void checkCalculationOfAverageGrade(Abiturient abiturient);
}
