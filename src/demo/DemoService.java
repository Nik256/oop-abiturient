package demo;

import entity.Abiturient;
import service.IAbiturientService;

import java.util.Arrays;

public class DemoService implements IDemoService {
    private IAbiturientService abiturientService;

    public DemoService(IAbiturientService abiturientService) {
        this.abiturientService = abiturientService;
    }

    public void execute() {
        Abiturient[] abiturients = new Abiturient[10];
        fillAbiturients(abiturients);
        Abiturient abiturient = new Abiturient("Miranda Klemm", new int[] {5, 3, 4, 5, 5});
        checkCalculationOfAverageGrade(abiturient);
        checkGettingAbiturientsWhoEnteredToTheUniversity(abiturients, 7);
    }

    public void fillAbiturients(Abiturient[] abiturients) {
        System.out.println("Abiturients:");
        abiturients[0] = new Abiturient("Haywood Vanek", new int[] {5, 5, 5, 5, 5});
        abiturients[1] = new Abiturient("Garry Faz", new int[] {4, 5, 3, 2, 3});
        abiturients[2] = new Abiturient("Sonia Friedrichs", new int[] {3, 2, 3, 3, 2});
        abiturients[3] = new Abiturient("Rona Helsley", new int[] {5, 4, 5, 4, 5});
        abiturients[4] = new Abiturient("Eddy Olin", new int[] {2, 2, 3, 2, 3});
        abiturients[5] = new Abiturient("Juliann Lee", new int[] {3, 4, 4, 4, 4});
        abiturients[6] = new Abiturient("Lilla Runions", new int[] {5, 4, 5, 5, 5});
        abiturients[7] = new Abiturient("Mickey Pulaski", new int[] {2, 3, 3, 2, 2});
        abiturients[8] = new Abiturient("Juan Kennedy", new int[] {5, 5, 5, 3, 5});
        abiturients[9] = new Abiturient("Pat Rancourt", new int[] {4, 4, 4, 5, 4});
        System.out.println(Arrays.toString(abiturients));
    }

    @Override
    public void checkGettingAbiturientsWhoEnteredToTheUniversity(Abiturient[] abiturients, int totalNumberOfStudents) {
        System.out.println("---------------------------------------------------------------------------------------------------------");
        System.out.println("Students: ");
        System.out.println(Arrays.toString(abiturientService.getAbiturientsWhoEnteredToTheUniversity(abiturients, totalNumberOfStudents)));
    }

    @Override
    public void checkCalculationOfAverageGrade(Abiturient abiturient) {
        System.out.println("---------------------------------------------------------------------------------------------------------");
        abiturient.setAverageGrade(abiturientService.calculateAverageGrade(abiturient));
        System.out.println(abiturient);
    }
}
