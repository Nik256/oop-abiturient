package entity;

import java.util.Arrays;

public class Abiturient implements Comparable<Abiturient> {
    private String name;
    private int[] grades;
    private double averageGrade;

    public Abiturient(String name, int[] grades) {
        this.name = name;
        this.grades = grades;
    }

    public double getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(double averageGrade) {
        this.averageGrade = averageGrade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "Abiturient{" + "name='" + name + '\'' + ", grades=" + Arrays.toString(grades) + ", averageGrade=" + averageGrade + '}';
    }

    @Override
    public int compareTo(Abiturient o) {
        return Double.compare(o.averageGrade, this.averageGrade);
    }
}
