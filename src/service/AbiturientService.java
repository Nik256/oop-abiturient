package service;

import entity.Abiturient;

import java.util.Arrays;

public class AbiturientService implements IAbiturientService {
    @Override
    public Abiturient[] getAbiturientsWhoEnteredToTheUniversity(Abiturient[] abiturients, int totalNumberOfStudents) {
        if(abiturients.length < totalNumberOfStudents)
            return abiturients;
        for (Abiturient abiturient : abiturients)
            abiturient.setAverageGrade(calculateAverageGrade(abiturient));
        Arrays.sort(abiturients);
        return Arrays.copyOf(abiturients, totalNumberOfStudents);
    }

    @Override
    public double calculateAverageGrade(Abiturient abiturient) {
        int sumOfGrades = 0;
        for (int grade : abiturient.getGrades())
            sumOfGrades += grade;
        return (double) sumOfGrades / abiturient.getGrades().length;
    }
}
