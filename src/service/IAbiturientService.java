package service;

import entity.Abiturient;

public interface IAbiturientService {
    Abiturient[] getAbiturientsWhoEnteredToTheUniversity(Abiturient[] abiturients, int totalNumberOfStudents);
    double calculateAverageGrade(Abiturient abiturient);
}
